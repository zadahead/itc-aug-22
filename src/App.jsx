import { AlignStart, Between, Line, Rows } from "./UIKit/Layouts/Line/Line"
import './App.css';
import { Grid } from "./UIKit/Layouts/Grid/Grid";
import { Box } from "./UIKit/Layouts/Box/Box";
import { Button } from "@mui/material";
import { Btn } from "./UIKit/Element/Btn/Btn";

export const App = () => {
    return (
        <Grid>
            <header>
                <Between>
                    <h3>logo</h3>
                    <div className="responsive">
                        <Line>
                            <h3>item1</h3>
                            <h3>item2</h3>
                        </Line>
                    </div>
                </Between>
            </header>
            <section>
                <Rows>
                    <Box>
                        <Between>
                            <Line>
                                <h3>pic</h3>

                                <Rows>
                                    <h3>day 11</h3>
                                    <Line>
                                        <h3>ZadaheaD</h3>
                                        <h3>4 days ago</h3>
                                    </Line>
                                </Rows>
                            </Line>
                            <AlignStart>
                                <Btn>Hello</Btn>
                            </AlignStart>
                        </Between>
                    </Box>
                    <Box>
                        <Line>
                            <h2>hello</h2>
                            <Btn>Hello</Btn>
                        </Line>
                    </Box>
                </Rows>
            </section>
            <footer>footer</footer>
        </Grid>
    )
}